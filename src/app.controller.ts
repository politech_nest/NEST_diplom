import { Controller, Delete, Get, Post, Put } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  //Запуск из postman http://127.0.0.1:3000 выбираем метод get и тд
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post()
  postAll(): string {
    return "post data";
  }

  @Put()
  update(): string {
    return "put data";
  }

  @Delete()
  Delete(): string {
    return "delete data";
  }
}
