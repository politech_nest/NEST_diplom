import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { IUser } from "../interface/user";

export type UserDocument = HydratedDocument<User>;

@Schema()// 1. определяем коллекцию в бд маонго дб
export class User implements IUser {
    @Prop() 
    psw: string;//3. прописываем свойства и указываем тип

    @Prop()
    cardNumber: string;

    @Prop()
    login: string;

    @Prop()
    email: string;

    @Prop()
    role: string;

}
// 2. с помощью метода createForClass класса SchemaFactory создаём эту схему
export const UserSchema = SchemaFactory.createForClass(User);